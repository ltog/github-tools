#!/usr/bin/python3

"""
Copyright 2018 Lukas Toggenburger; https://gitlab.com/ltog/github-tools

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import argparse
import logging
import requests
import json
import time

def backoff_sleep(count):
    duration = 10*(2**count-1) # 0, 10, 30, 70, 150, 310, ...

    if (duration > 0):
        print("Duration of backoff sleep = " + str(duration))

    time.sleep(duration)

def generic_backoff_download(url):
    count = 0
    while True:
        backoff_sleep(count)
        count += 1

        r = None

        try:
            r = requests.get(url)
        except requests.exceptions.RequestException as e: # http://docs.python-requests.org/en/latest/user/quickstart/#errors-and-exceptions , e.g. 404 throws no exception
            print("Got exception " + str(e) + " for URL " + url + " , but will continue trying to fetch it.")
            continue

        r.encoding = 'utf-8' # if encoding is not set, python will spend quite some cpu time trying to determine the encoding when using r.text, see also https://github.com/kennethreitz/requests/issues/1604#issuecomment-24476927

        if r.status_code == 200:
            return r
        elif r.status_code == 403: # 404 generates no exception
            print("Error 403. Message: " + r.text)


def main(args):
    token="" # TODO: generate token at https://github.com/settings/tokens
    augmented_issues = []
    nonaugmented_issues = []


    for page in range(1, 31): # TODO: don't hardcode the number of pages, but instead read values from http header according to https://developer.github.com/v3/guides/traversing-with-pagination/#basics-of-pagination
    #for page in range(1, 3):
        print(page, end="", flush=True)
        r = generic_backoff_download("https://api.github.com/repos/mapillary/mapillary_issues/issues?state=all&page=" + str(page) + "&per_page=100&access_token=" + token)
        r.encoding = 'utf-8' # if encoding is not set, python will spend quite some cpu time trying to determine the encoding when using r.text, see also https://github.com/kennethreitz/requests/issues/1604#issuecomment-24476927
        #print(json.dumps(r.json()[3], indent=4))  #TODO: remove
        issues = r.json()
        for issue in issues:
            print(".", end="", flush=True)
            #print("issue=" + str(issue))
            nonaugmented_issues.append(issue)

            if issue["comments"] > 0:
                comments_url = issue["comments_url"]
                r2 = generic_backoff_download(comments_url + "?access_token=" + token)
                issue["comments_dump"] = r2.json()

            augmented_issues.append(issue)

    #print(str(issues))

    with open("mapillary-issues.json", "w") as f:
        json.dump(nonaugmented_issues, f)

    with open("mapillary-issues-augmented.json", "w") as f:
        json.dump(augmented_issues, f)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Download GitHub issues including comments')

    args = parser.parse_args()

    logging.getLogger("requests").setLevel(logging.WARNING) # disable messages from module "requests", unless it's at least a warning, see http://stackoverflow.com/a/11029841
    logging.getLogger("urllib3").setLevel(logging.WARNING) # disable messages from module "urllib3" (used by "requests"), unless it's at least a warning, see http://stackoverflow.com/a/11029841

    main(args)


